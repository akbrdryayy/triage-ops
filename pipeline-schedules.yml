---
# Across different groups and projects, defined in the jobs
default-labels:
  variants:
    - active: true
      description: '[DAILY] defaults labels for groups and projects'
      variables:
        # **DO NOT** DEFINE TRIAGE_SOURCE_PATH for this schedule
        # We're relying on the jobs here.
        # pre-hygiene
        TRIAGE_CHARTS_DEFAULT_LABELS: 1
        TRIAGE_GITTER_DEFAULT_LABELS: 1
        TRIAGE_CNG_DEFAULT_LABELS: 1
        TRIAGE_CUSTOMER_AND_LICENSE_DEFAULT_LABELS: 1
        TRIAGE_GITALY_DEFAULT_LABELS: 1
        TRIAGE_GITLAB_RUNNER_DEFAULT_LABELS: 1
        TRIAGE_CHARTS_GITLAB_RUNNER_DEFAULT_LABELS: 1
        TRIAGE_CHARTS_AUTO_DEPLOY_DEFAULT_LABELS: 1
        TRIAGE_OMNIBUS_GITLAB_DEFAULT_LABELS: 1
        TRIAGE_TESTCASES_DEFAULT_LABELS: 1
        TRIAGE_DISTRIBUTION_DEFAULT_LABELS: 1
        TRIAGE_GITLAB_ORCHESTRATOR_DEFAULT_LABELS: 1
        TRIAGE_MERGE_REQUEST_AUTHOR_GROUP_LABEL: 1
        # hygiene
        TRIAGE_STAGE_AND_GROUP_LABELS_HYGIENE: 1

missed-resources:
  variants:
    - active: true
      description: '[DAILY] Add missed labels for projects'
      cron: 5 0 * * *
      cron_timezone: America/Los_Angeles
      variables:
        TZ: America/Los_Angeles
        # **DO NOT** DEFINE TRIAGE_SOURCE_PATH for this schedule
        # We're relying on the jobs here.
        TRIAGE_MISSED_RESOURCES: 1

# Groups
gitlab-org/charts:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 5032027
  variants:
    - id: 39481 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/39481/edit
      active: true
      description: '[DAILY] gitlab-org/charts'
      variables:
        # hygiene
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1

gitlab-org/distribution:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 2639717
  variants:
    - active: true
      description: '[DAILY] gitlab-org/distribution'
      variables:
        # hygiene
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1

gitlab-org:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 9970
  variants:
    - id: 11219 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/11219/edit
      active: true
      description: '[DAILY] gitlab-org'
      variables:
        # hygiene
        TRIAGE_DISCOVER: 1
        TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS: 1
        # close-reports
        TRIAGE_CLOSE_OLD_TRIAGE_REPORTS: 1
        TRIAGE_CLOSE_REPORTS_RESOURCES_MOVED_TO_NEXT_MILESTONE: 1
    - id: 59186 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/59186/edit
      active: true
      description: '[WEEKLY] gitlab-org'
      cron: '0 2 * * 1'
      variables:
        # report
        TRIAGE_EP_REPORT: 1

# Projects
gitlab-org/quality/triage-ops:
  base:
    active: true
  variants:
    - id: 32396 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/32396/edit
      description: '[6-hourly] gitlab-org/quality/triage-ops regular pipeline run'
      cron: '0 */6 * * *'
      variables:
        SCHEDULED_TESTS: 1

gitlab-com/www-gitlab-com:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 7764
  variants:
    - id: 18297 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/18297/edit
      active: true
      description: '[DAILY] gitlab-com/www-gitlab-com'
      variables:
        # hygiene
        TRIAGE_WEBSITE_LABEL_COMMUNITY_CONTRIBUTIONS: 1

gitlab-org/customers-gitlab-com:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 2670515
  variants:
    - id: 35933 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/35933/edit
      active: true
      description: '[DAILY] gitlab-org/customers-gitlab-com'
      variables:
        # hygiene
        TRIAGE_CLOSE_OLD_ISSUES: 1

gitlab-org/gitaly:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 2009901
  variants:
    - id: 29054 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29054/edit
      active: true
      description: '[DAILY] gitlab-org/gitaly'
      variables:
        # hygiene
        TRIAGE_LABEL_ACCEPTING_MERGE_REQUESTS: 1
        TRIAGE_LABEL_MISSED_SLO: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1
    - id: 29055 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29055/edit
      active: true
      description: '[WEEKLY] gitlab-org/gitaly'
      cron: '0 2 * * 1'
      variables:
        # report
        TRIAGE_COMMUNITY_MERGE_REQUESTS: 1
        TRIAGE_MISSING_CATEGORIES: 1
        TRIAGE_TEAM_SUMMARY: 1

gitlab-org/gitlab-foss:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 13083
  variants:
    - id: 33854 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/33854/edit
      active: true
      description: '[3-hour] gitlab-org/gitlab-foss migrate open gitlab-foss issues to gitlab'
      cron: '0 */3 * * *'
      variables:
        # hygiene
        TRIAGE_GITLAB_FOSS_MIGRATE_ISSUES_TO_GITLAB: 1

gitlab-org/gitlab-qa:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 1441932
  variants:
    - id: 29050 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29050/edit
      active: true
      description: '[DAILY] gitlab-org/gitlab-qa'
      variables:
        # hygiene
        TRIAGE_LABEL_ACCEPTING_MERGE_REQUESTS: 1
        TRIAGE_LABEL_MISSED_SLO: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1
    - id: 29051 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29051/edit
      active: true
      description: '[WEEKLY] gitlab-org/gitlab-qa'
      cron: '0 2 * * 1'
      variables:
        # report
        TRIAGE_COMMUNITY_MERGE_REQUESTS: 1
        TRIAGE_MISSING_CATEGORIES: 1

gitlab-org/gitlab-runner:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 250833
  variants:
    - id: 29681 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29681/edit
      active: true
      description: '[DAILY] gitlab-org/gitlab-runner'
      variables:
        # hygiene
        TRIAGE_LABEL_ACCEPTING_MERGE_REQUESTS: 1
        TRIAGE_LABEL_MISSED_SLO: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1
    - id: 29680 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29680/edit
      active: true
      description: '[WEEKLY] gitlab-org/gitlab-runner'
      cron: '0 2 * * 1'
      variables:
        # report
        TRIAGE_COMMUNITY_MERGE_REQUESTS: 1
        TRIAGE_MISSING_CATEGORIES: 1
        TRIAGE_TEAM_SUMMARY: 1

gitlab-org/gitlab:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 278964
  variants:
    - id: 10515 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/10515/edit
      active: true
      description: '[DAILY] gitlab-org/gitlab'
      variables:
        # hygiene
        TRIAGE_ADD_MILESTONE_TO_COMMUNITY_MERGE_REQUESTS: 1
        TRIAGE_LABEL_ACCEPTING_MERGE_REQUESTS: 1
        TRIAGE_LABEL_MISSED_SLO: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1
        TRIAGE_UNLABELLED_ISSUES: 1
    - id: 11987 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/11987/edit
      active: true
      cron: '0 2 * * 1'
      description: '[WEEKLY] gitlab-org/gitlab'
      variables:
        # report
        TRIAGE_COMMUNITY_MERGE_REQUESTS: 1
        TRIAGE_MISSING_CATEGORIES: 1
        TRIAGE_TEAM_SUMMARY: 1
        TRIAGE_FLAKY_EXAMPLES_REPORT: 1
        TRIAGE_QUALITY_REPORT: 1
    - active: true
      cron: '0 2 8,23 * *'
      description: '[On 8th and 23nd] gitlab-org/gitlab'
      variables:
        TRIAGE_IDLE_TEAM_MERGE_REQUESTS: 1

gitlab-org/license-gitlab-com:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 6457868
  variants:
    - id: 35934 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/35934/edit
      description: '[DAILY] gitlab-org/license-gitlab-com'
      active: true
      variables:
        # hygiene
        TRIAGE_CLOSE_OLD_ISSUES: 1

gitlab-org/omnibus-gitlab:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 20699
  variants:
    - id: 31880 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/31880/edit
      active: true
      description: '[DAILY] gitlab-org/omnibus-gitlab'
      variables:
        # hygiene
        TRIAGE_LABEL_ACCEPTING_MERGE_REQUESTS: 1
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1

gitlab-org/plan:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 13453461
  variants:
    - id: 39392
      active: true
      description: '[1st & 15th every month] gitlab-org/plan refinement cadence'
      cron: '0 4 1,15 * *'
      variables:
        # hygiene
        TRIAGE_PLAN_STAGE_BACKEND_FRONTEND_CHECK: 1
    - id: 39393 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/39393/edit
      active: true
      description: '[8th & 22nd every month] gitlab-org/plan group label check'
      cron: '0 4 8,22 * *'
      variables:
        TRIAGE_PLAN_STAGE_GROUP_LABEL_CHECK: 1
    - id: 39394 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/39394/edit
      active: true
      description: '[18th every month] gitlab-org/plan end of release refinement'
      cron: '0 4 18 * *'
      variables:
        TRIAGE_PLAN_STAGE_END_OF_RELEASE_REFINEMENT: 1
    - id: 39395 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/39395/edit
      active: true
      description: '[29th every month] gitlab-org/plan security issue check'
      cron: '0 4 29 * *'
      variables:
        TRIAGE_PLAN_STAGE_SECURITY_ISSUE_CHECK: 1

gitlab-org/gitlab-orchestrator:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 14378900
  variants:
    - active: true
      description: '[DAILY] gitlab-org/gitlab-orchestrator'
      variables:
        # hygiene
        TRIAGE_LABEL_REMINDERS: 1
        TRIAGE_MOVE_MILESTONE_FORWARD: 1

gitlab-com/support-forum:
  base:
    variables:
      TRIAGE_SOURCE_TYPE: projects
      TRIAGE_SOURCE_PATH: 63904
  variants:
    - id: 51638 # https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/51638/edit
      active: true
      description: '[DAILY] gitlab-com/support-forum'
      variables:
        # report
        TRIAGE_SUPPORT_FORUM_AUTO_CLOSURE: 1
