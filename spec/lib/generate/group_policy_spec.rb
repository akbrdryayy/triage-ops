# frozen_string_literal: true

require 'spec_helper'
require 'generate/group_policy'

RSpec.describe Generate::GroupPolicy do
  let(:options) { double(:options, template: template_path) }
  let(:template_path) { 'path/to/template' }
  let(:template_name) { File.basename(template_path) }
  let(:output_dir) { "#{described_class.destination}/#{template_name}" }
  let(:fake_group) { 'fake' }

  describe '.run' do
    it 'generates and writes policy files' do
      expect(File)
        .to receive(:read)
        .with(template_path)
        .and_return(<<~ERB)
          <%= group_method_name %>
          <%= group_label_name %>
        ERB

      expect(FileUtils)
        .to receive(:mkdir_p)
        .with(output_dir)

      stub_const('GroupDefinition::DATA', { fake_group => {} })

      expect(GroupDefinition)
        .to receive(:public_send)
        .with("group_#{fake_group}")
        .and_return({ labels: ["group::#{fake_group}"] })

      expect(File)
        .to receive(:write)
        .with(
          "#{output_dir}/#{fake_group}.yml",
          <<~MARKDOWN)
            group_#{fake_group}
            group::#{fake_group}
          MARKDOWN

      described_class.run(options)
    end
  end
end
