# frozen_string_literal: true

require 'spec_helper'
require 'generate/option'

RSpec.describe Generate::Option do
  describe '.parse' do
    let(:template_path) { 'path/to/template' }

    it 'parses -t to set template' do
      options = described_class.parse(['-t', template_path])

      expect(options.template).to eq(template_path)
    end

    it 'parses --template to set template' do
      options = described_class.parse(['--template', template_path])

      expect(options.template).to eq(template_path)
    end
  end
end
