.common_conditions_for_removing_label_from_issues: &common_conditions_for_removing_label_from_issues
  labels:
    - Accepting merge requests
  ruby: resource[:assignees].any?
  state: opened

.common_actions_for_removing_label: &common_actions_for_removing_label
  remove_labels:
    - Accepting merge requests

resource_rules:
  issues:
    rules:
      - name: Apply `Accepting merge requests` to unassigned, unblocked issues with a milestone
        conditions:
          milestone: any
          forbidden_labels:
            - Accepting merge requests
            - workflow::blocked
          state: opened
          # There's currently a bug with the milestone=any API filter:
          # https://gitlab.com/gitlab-org/gitlab-foss/issues/65676
          ruby: |
            return false unless milestone

            resource[:assignees].empty?
        actions:
          labels:
            - Accepting merge requests

      - name: Remove `Accepting merge requests` from all issues assigned to a gitlab-org member
        conditions:
          <<: *common_conditions_for_removing_label_from_issues
          assignee_member:
            source: group
            condition: member_of
            # gitlab-org
            source_id: 9970
          ruby: |
            return false if milestone && milestone.title == "Backlog"

            resource[:assignees].any?
        actions:
          <<: *common_actions_for_removing_label

      - name: Remove `Accepting merge requests` from all issues assigned to community contributor
        conditions:
          <<: *common_conditions_for_removing_label_from_issues
          assignee_member:
            source: group
            condition: not_member_of
            # gitlab-org
            source_id: 9970
        actions:
          <<: *common_actions_for_removing_label
          comment: |
            Thanks for working on this {{assignee}}! We've removed the ~"Accepting merge requests" label to avoid having multiple people working on the same issue.

      - name: Remove `Accepting merge requests` from all blocked issues
        conditions:
          <<: *common_conditions_for_removing_label_from_issues
          labels:
            - Accepting merge requests
            - workflow::blocked
        actions:
          <<: *common_actions_for_removing_label

  merge_requests:
    rules:
      - name: Remove `Accepting merge requests` from all merge requests
        conditions:
          labels:
            - Accepting merge requests
        actions:
          <<: *common_actions_for_removing_label
