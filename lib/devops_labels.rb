# frozen_string_literal: true

require_relative 'www_gitlab_com'

class DevopsLabels
  CATEGORIES_PER_DEPARTMENT = {
    'Quality' => [
      'failure::broken-test',
      'failure::flaky-test',
      'failure::stale-test',
      'failure::test-environment',
      'master:broken',
      'master:needs-investigation',
      'QA'
    ],
    'Engineering Productivity' => [
      'ci-build',
      'insights',
      'dev-quality',
      'static analysis'
    ]
  }
  DEPRECATED_TEAM_TO_STAGE = {
    'Manage [DEPRECATED]' => 'manage',
    'Plan [DEPRECATED]' => 'plan',
    'Create [DEPRECATED]' => 'create'
  }
  STAGE_LABEL_PREFIX = 'devops::'
  GROUP_LABEL_PREFIX = 'group::'
  INFRASTRUCTURE_TEAM_LABEL_PREFIX = 'team::'
  CATEGORY_LABEL_PREFIX = 'Category:'
  DISABLED_SINGLE_CATEGORY_TO_GROUP_INFERENCE = ['Category:Navigation'].freeze

  def self.stages
    WwwGitLabCom.stages_from_www.keys
  end

  def self.groups
    groups_per_stage.values.flatten
  end

  def self.category_labels_for_www_categories(categories)
    www_category_keys = (categories || []).select do |category_key|
      WwwGitLabCom.categories_from_www.key?(category_key)
    end

    www_category_keys.each_with_object([]) do |category_key, memo|
      category = WwwGitLabCom.categories_from_www[category_key]
      memo <<
        if category.key?('label')
          category['label']
        else
          "#{CATEGORY_LABEL_PREFIX}#{category['name']}"
        end
      memo << category['feature_labels'] if category.key?('feature_labels')
    end.flatten
  end

  private_class_method :category_labels_for_www_categories

  def self.groups_per_stage
    @groups_per_stage ||=
      WwwGitLabCom.stages_from_www.each_with_object({}) do |(stage, attrs), memo|
        memo[stage] = attrs['groups'].each_key.map { |group| group.tr('_', ' ') }
      end
  end

  def self.categories_per_group
    @categories_per_group ||=
      WwwGitLabCom.groups_from_www.each_with_object({}) do |(group, attrs), memo|
        group_key = group.tr('_', ' ')
        memo[group_key] = attrs.fetch('feature_labels', [])
        memo[group_key].concat(category_labels_for_www_categories(attrs['categories']))
      end
  end

  def self.categories_per_group_or_department
    @categories_per_group_or_department ||= categories_per_group.merge(CATEGORIES_PER_DEPARTMENT)
  end

  def self.stage_labels
    @stage_labels ||=
      stages.map do |stage|
        "#{STAGE_LABEL_PREFIX}#{stage}"
      end
  end

  def self.stage_and_departments_labels
    @stage_and_departments_labels ||= stage_labels + departments
  end

  def self.group_labels
    @group_labels ||=
      groups.map do |group|
        "#{GROUP_LABEL_PREFIX}#{group}"
      end
  end

  def self.category_labels
    @category_labels ||=
      categories_per_group.values.flatten +
        CATEGORIES_PER_DEPARTMENT.values.flatten
  end

  def self.deprecated_team_labels
    @deprecated_team_labels ||= DEPRECATED_TEAM_TO_STAGE.keys
  end

  def self.departments
    @departments ||= CATEGORIES_PER_DEPARTMENT.keys
  end

  def self.department?(item)
    departments.include?(item)
  end

  def self.teams_per_user(username)
    gitlab_team_member = WwwGitLabCom.team_from_www[username]
    return [] if gitlab_team_member.nil?

    departments = gitlab_team_member['departments']
    return [] if departments.nil?

    teams = departments.select { |dept| dept.end_with?('Team') }
    sanitize_team_names(teams)
  end

  def self.sanitize_team_names(teams)
    teams.map do |team|
      team_name = team.delete_suffix(' Team').downcase
      team_name = drop_role(team_name)
      team_name = drop_stage(team_name)

      team_name
    end
  end

  private_class_method :sanitize_team_names

  def self.drop_role(team_name)
    %w[ux be fe pm].each do |role|
      return team_name.delete_suffix(" #{role}") if team_name.end_with?(role)
    end

    team_name
  end

  private_class_method :drop_role

  def self.drop_stage(team_name)
    stages.each do |stage|
      return team_name.delete_prefix("#{stage}:") if team_name.start_with?(stage)
    end

    team_name
  end

  private_class_method :drop_stage

  module Context
    extend self

    LabelsInferenceResult = Struct.new(:new_labels, :matching_labels) do
      def any?
        new_labels.any?
      end

      def new_labels
        self[:new_labels] ? Array[self[:new_labels]].flatten.compact : []
      end

      def matching_labels
        self[:matching_labels] ? Array[self[:matching_labels]].flatten : []
      end

      def comment
        "#{explanation}\n#{quick_actions}"
      end

      def quick_actions
        "/label #{new_labels_markdown}"
      end

      def self.merge(*label_inference_results)
        new_labels = label_inference_results.map(&:new_labels).uniq
        matching_labels = label_inference_results.map(&:matching_labels).uniq

        self.new(new_labels, matching_labels)
      end

      private

      def new_labels_markdown
        new_labels.map(&Context.method(:markdown_label)).join(' ')
      end

      def matching_labels_markdown
        matching_labels.map(&Context.method(:markdown_label)).join(' ')
      end

      def explanation
        "Setting label(s) #{new_labels_markdown} based on #{matching_labels_markdown}." if any?
      end
    end

    MatchingGroup = Struct.new(:name, :matching_labels) do
      def in_stage?(stage)
        Context.stage_for_group(name) == stage
      end

      def markdown_labels
        matching_labels.map(&Context.method(:markdown_label)).join(' ')
      end
    end

    MergeRequestAuthorGroupLabel = Struct.new(:author, :label) do
      def comment
        "#{explanation}\n#{quick_actions}"
      end

      private

      def explanation
        "Setting label #{markdown_label} based on `@#{author}`'s group."
      end

      def quick_actions
        "/label #{markdown_label}"
      end

      def markdown_label
        Context.markdown_label(label)
      end
    end

    def label_names
      @label_names ||= labels.map(&:name)
    end

    def current_stage_label
      (DevopsLabels.stage_and_departments_labels & label_names).first
    end

    def current_group_label
      (DevopsLabels.group_labels & label_names).first
    end

    def current_infrastructure_team_label
      label_names.find { |label| label.start_with?(INFRASTRUCTURE_TEAM_LABEL_PREFIX) }
    end

    def current_category_labels
      DevopsLabels.category_labels & label_names
    end

    def current_deprecated_team_label
      (DevopsLabels.deprecated_team_labels & label_names).first
    end

    def current_stage_name
      return unless current_stage_label

      current_stage_label.delete_prefix(DevopsLabels::STAGE_LABEL_PREFIX)
    end

    def current_group_name
      return unless current_group_label

      current_group_label.delete_prefix(DevopsLabels::GROUP_LABEL_PREFIX)
    end

    def all_category_labels_for_stage(stage)
      return [] unless DevopsLabels.stages.include?(stage)

      DevopsLabels.groups_per_stage[stage].flat_map do |group|
        all_category_labels_for_group(group)
      end
    end

    def all_category_labels_for_group(group)
      DevopsLabels.categories_per_group.fetch(group, [])
    end

    def has_stage_label?
      !current_stage_label.nil?
    end

    def has_group_label?
      !current_group_label.nil?
    end

    def has_infrastructure_team_label?
      !current_infrastructure_team_label.nil?
    end

    def has_category_label?
      current_category_labels.any?
    end

    def has_category_label_for_current_stage?
      return false unless has_stage_label?

      !(all_category_labels_for_stage(current_stage_name) & label_names).empty?
    end

    def has_category_label_for_current_group?
      return false unless has_group_label?

      !(all_category_labels_for_group(current_group_name) & label_names).empty?
    end

    def can_infer_labels?
      !!(current_stage_label || current_group_label || !current_category_labels.empty? || current_deprecated_team_label)
    end

    def can_infer_group_from_author?
      !!group_for_user(author)
    end

    def single_deprecated_team_label?
      (DevopsLabels.deprecated_team_labels & label_names).one?
    end

    alias_method :can_infer_from_deprecated_team_label?, :single_deprecated_team_label?

    def stage_has_a_single_group?(stage)
      return false unless DevopsLabels.groups_per_stage.key?(stage)

      DevopsLabels.groups_per_stage[stage].one?
    end

    def group_part_of_stage?
      return false unless current_stage_name && current_group_name

      DevopsLabels.groups_per_stage[current_stage_name]&.include?(current_group_name)
    end


    def first_group_for_stage(stage)
      DevopsLabels.groups_per_stage[stage]&.first
    end

    def stage_for_group(group)
      detected_stage = DevopsLabels.groups_per_stage.detect do |stage, groups|
        groups.include?(group)
      end

      detected_stage&.first
    end

    def new_stage_and_group_labels_from_intelligent_inference(infer_from_category: true)
      new_labels_from_single_deprecated_team_label = infer_stage_label_from_deprecated_team_label

      return new_labels_from_single_deprecated_team_label if new_labels_from_single_deprecated_team_label.any?

      infer_new_labels(infer_from_category) || LabelsInferenceResult.new
    end

    def comment_for_intelligent_stage_and_group_labels_inference(infer_from_category: true)
      new_stage_and_group_labels = new_stage_and_group_labels_from_intelligent_inference(infer_from_category: infer_from_category)

      new_stage_and_group_labels.comment if new_stage_and_group_labels.any?
    end

    def inference_strategy_for_issue
      return if has_stage_label? && has_group_label? && has_category_label_for_current_group?

      if has_stage_label?
        if has_group_label?
          :infer_category_from_group
        else
          :infer_group_from_category
        end
      else
        if has_group_label?
          :infer_stage_and_category_from_group
        else
          :infer_stage_and_group_from_category
        end
      end
    end

    def inference_strategy_for_merge_request
      return if has_stage_label? && has_group_label? && has_category_label?

      if has_stage_label?
        if has_group_label?
          if group_part_of_stage?
            :infer_category_from_group
          end
        else
          :infer_group_from_stage
        end
      end
    end

    def group_for_user(username)
      return if username.nil? || username.empty?

      teams = DevopsLabels.teams_per_user(username)
      matching_groups = teams.select do |team|
        DevopsLabels.groups.include?(team)
      end

      matching_groups.first if matching_groups.one?
    end

    def comment_for_mr_author_group_label
      group = group_for_user(author)

      return if group.nil?

      label = label_for(group: group)

      MergeRequestAuthorGroupLabel.new(author, label).comment
    end

    def markdown_label(label)
      %(~"#{label}")
    end

    private

    def infer_new_labels(infer_from_category)
      strategy = infer_from_category ? inference_strategy_for_issue : inference_strategy_for_merge_request
      return if strategy.nil?

      self.__send__(strategy)
    end

    def infer_group_from_stage
      return unless stage_has_a_single_group?(current_stage_name)

      LabelsInferenceResult.new(label_for(group: first_group_for_stage(current_stage_name)), current_stage_label)
    end

    def infer_group_from_category
      matching_group = best_matching_group

      if matching_group && matching_group.in_stage?(current_stage_name)
        LabelsInferenceResult.new(label_for(group: matching_group.name), matching_group.matching_labels)
      elsif stage_matching_groups.none?
        infer_group_from_stage
      end
    end

    def infer_stage_and_group_from_category
      matching_group = best_matching_group
      return unless matching_group

      stage_label = label_for(stage: stage_for_group(matching_group.name))
      group_label = label_for(group: matching_group.name)

      LabelsInferenceResult.new([stage_label, group_label], matching_group.matching_labels)
    end

    def infer_stage_label_from_deprecated_team_label
      return LabelsInferenceResult.new unless single_deprecated_team_label?

      stage = stage_from_deprecated_team_label unless has_stage_label?

      if stage
        stage_label = label_for(stage: stage)

        LabelsInferenceResult.new([stage_label], current_deprecated_team_label)
      else
        LabelsInferenceResult.new
      end
    end

    def infer_category_from_group
      category_labels = all_category_labels_for_group(current_group_name).filter do |category|
        category.start_with?(DevopsLabels::CATEGORY_LABEL_PREFIX)
      end

      if category_labels.one? && !DISABLED_SINGLE_CATEGORY_TO_GROUP_INFERENCE.include?(category_labels.first)
        LabelsInferenceResult.new(category_labels.first, current_group_label)
      else
        LabelsInferenceResult.new
      end
    end

    def infer_stage_from_group
      stage_label = label_for(stage: stage_for_group(current_group_name))

      LabelsInferenceResult.new(stage_label, current_group_label)
    end

    def infer_stage_and_category_from_group
      inferred_stage = infer_stage_from_group
      inferred_category = infer_category_from_group

      LabelsInferenceResult.merge(inferred_stage, inferred_category)
    end

    def best_matching_group
      matching_groups = stage_matching_groups.any? ? stage_matching_groups : groups_from_category
      matching_labels_count = matching_groups.map(&:matching_labels).size.to_f

      matching_groups.find do |matching_group|
        (matching_group.matching_labels.size / matching_labels_count) > 0.5
      end
    end

    def groups_from_category
      @groups_from_category ||=
        DevopsLabels.categories_per_group_or_department.each_with_object([]) do |(group, category_labels), matches|
          matching_category_labels = category_labels & label_names
          matches << MatchingGroup.new(group, matching_category_labels) if matching_category_labels.any?
        end
    end

    def stage_matching_groups
      @stage_matching_groups ||=
        groups_from_category.select do |matching_group|
          DevopsLabels.department?(matching_group.name) ||
            stage_for_group(matching_group.name) == current_stage_name
        end
    end

    def stage_from_deprecated_team_label
      DEPRECATED_TEAM_TO_STAGE.find do |(team, _), matches|
        label_names.include?(team)
      end&.last
    end

    def label_for(stage: nil, group: nil)
      stage_or_group = stage || group
      return stage_or_group if DevopsLabels.department?(stage_or_group)

      if stage
        "#{DevopsLabels::STAGE_LABEL_PREFIX}#{stage}"
      elsif group
        "#{DevopsLabels::GROUP_LABEL_PREFIX}#{group}"
      end
    end
  end
end
