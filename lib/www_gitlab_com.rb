# frozen_string_literal: true

require 'yaml'
require 'httparty'

class WwwGitLabCom
  WWW_GITLAB_COM_STAGES_YML = 'https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/stages.yml'
  WWW_GITLAB_COM_CATEGORIES_YML = 'https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/categories.yml'
  WWW_GITLAB_COM_TEAM_YML = 'https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml'

  def self.stages_from_www
    @stages_from_www ||= fetch_yml(WWW_GITLAB_COM_STAGES_YML)['stages']
  end

  def self.groups_from_www
    @groups_from_www ||=
      stages_from_www.each_with_object({}) do |(stage, attrs), memo|
        memo.merge!(attrs['groups'])
      end
  end

  def self.categories_from_www
    @categories_from_www ||= fetch_yml(WWW_GITLAB_COM_CATEGORIES_YML)
  end

  def self.team_from_www
    @team_member_from_www ||=
      fetch_yml(WWW_GITLAB_COM_TEAM_YML).each_with_object({}) do |item, memo|
        memo.merge!({ item['gitlab'] => item })
      end
  end

  def self.fetch_yml(yaml_url)
    YAML.load(request_yaml(yaml_url))
  end
  private_class_method :fetch_yml

  def self.request_yaml(yaml_url)
    HTTParty.get(yaml_url) # Make sure this can follow redirect
  end
  private_class_method :request_yaml
end
